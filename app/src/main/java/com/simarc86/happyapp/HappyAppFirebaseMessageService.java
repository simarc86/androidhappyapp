package com.simarc86.happyapp;

import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class HappyAppFirebaseMessageService extends FirebaseMessagingService {
    private static final String TAG = HappyAppFirebaseMessageService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);//cj02USk3BjE:APA91bFaJ1_nClOQA1h_pipS-tYy_IIF7Z88XEUrL9OTvEcqU4EReHp8UPTLHg3TSkifc6NDwqMaN0LL1B4LNXyUZdI_YF92hem7Fd9r_WLeJJ62Nj4TTOkhDjOkuFvi_ZFJOBSoYnrO
        Log.d(TAG, "Refreshed token: " + refreshedToken);
//dyokNcvy_9c:APA91bFlYAJ-KI68kvmaj45Ok4eI0ZTWr4ZcObx5AxvsQcDsbFquj_r3AwgRWSU997pXp3U0ikm3KCdxM8KgUKs86tu_adKWKuZYElGFebLD736YeJbg65qsW-VhRZ0X9FEOSsyI5wgg
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //new Handler(Looper.getMainLooper()).post(() -> sendRegistrationToServer(refreshedToken));

    }


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        Map<String, String> data = remoteMessage.getData();
        String myCustomKey = data.get("video");

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage);
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message
     *
     * @param remoteMessage remoteMessage which has the message data in it
     */
    private void sendNotification(RemoteMessage remoteMessage) {


        NotificationHandler notificationHandler = new NotificationHandler();
        notificationHandler.parseNotification(remoteMessage.getData());

        String link = remoteMessage.getData().get("video");
        YouTubeHandler.Companion.openLink(this, link);


    }
}
