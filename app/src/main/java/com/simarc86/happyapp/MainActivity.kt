package com.simarc86.happyapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val bundle = intent.extras
        if (bundle != null) {
            //TODO: send extras to notification Handler
            //TODO: get type of notification
            //TODO: open appropiate activity
            YouTubeHandler.openLink(this, bundle.getString("video"))
        }
    }

}
