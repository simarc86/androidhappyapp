package com.simarc86.happyapp

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri



class YouTubeHandler{
    companion object {
        fun openLink(context: Context, link: String){
            val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(link)
            )
            try {
                context.startActivity(appIntent)
            } catch (ex: ActivityNotFoundException) {
                context.startActivity(webIntent)
            }
        }
    }
}